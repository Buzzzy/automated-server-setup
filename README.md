Automated Server Setup

THIS WILL NOT WORK ON SHARED HOSTING! YOU MUST HAVE ROOT SHELL (SSH) ACCESS IN ORDER TO RUN.


This is a script that will install all the things you need to create a minecraft server on a linux.  The download is a zip file and when you open the zip you will see two directories. One called Debian, the other Centos. If you are running any Debian based system (Ubuntu etc..) open that and upload those files to the root directory of your server and follow instructions listed below. The second directory is called Centos, this is for Centos/Rhel systems. There will be 3 files inside that directory and upload them to the root of the server. Then follow the directions below. which will allow you to always be able to download the latest version of the script that will update your start script and spigot jar to the latest versions. Both of those scripts have been tested and are working on the following version per their chosen distro:

Debian Script
Debian 8 Jessie

CentOS Script
Centos 7

Ubuntu Script
Ubuntu 16.04 LTS Server

This script will always download and be updated to the latest spigot release. If you would like a custom script made please contact me at my email: admin@buzzzy.co please report all bugs with the script here, and I will try to fix. Please keep in mind if it is not that distro it may not work. If you would like me to add support to a distro please dm me and I will look into it. This script is designed to help make having a minecraft server without a panel easier and for everyone. Please keep in mind that I am a student so it may take me a little bit to respond to your requests. I will be working on adding support for more versions, but it takes time as I have to research it, find out the commands, download an iso and create a vm to test it.

Install instructions
1. Download the file.
2. Upload it via sftp or ftp to the root directory of the server.
3. Login to the server as either root or a user with sudo privileges.
4. Run the following command: chmod 770 ./serversetup.sh and then ./serversetup.sh to run the script!  [/SPOILER]

Video Tutorials

Coming soon

This script will:
Install screens
Download the latest jar from spigot
Download a startup script
Agree to the EULA
Start the server
Download a few basic plugins if you choose to.
Install lamp and phpmyadmin (Debian/Ubuntu only)

This script will not:
Configure plugins
Setup Bungeecord


Terms and
Conditions
You automatically agree to these when you download the files.
You may edit this script for personal use.
You cannot redistribute this script without permission of me.
You may not upload this script to any file sharing service without my permission.